#if UNITY_EDITOR

using System;

using UnityEditor;
using UnityEditor.Rendering.PostProcessing;

using UnityEngine;

[Decorator(typeof(LayerAttribute))]
internal sealed class LayerDecorator : AttributeDecorator
{
    public override bool OnGUI(
        SerializedProperty property,
        SerializedProperty overrideState,
        GUIContent         title,
        Attribute          attribute
    )
    {
        var attr = (LayerAttribute) attribute;

        if (property.propertyType == SerializedPropertyType.Integer)
        {
            property.intValue = EditorGUILayout.LayerField(title, property.intValue);
            return true;
        }
        
        return false;
    }
}

#endif

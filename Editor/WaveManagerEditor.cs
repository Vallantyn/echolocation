﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEditor;

using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.Rendering.PostProcessing;

#if UNITY_EDITOR

[CustomEditor(typeof(WaveManager))]
public class WaveManagerEditor : Editor
{
    float RadiusScaler(
        Vector3        InPos,
        Vector3        InDir,
        FloatParameter InRadius,
        string         InName,
        float          InSize,
        float          InSnap,
        Color          handleColor,
        Color          labelColor
    )
    {
        var      handlesColor = Handles.color;
        GUIStyle style        = new GUIStyle();

        style.normal.textColor = labelColor;
        style.richText         = true;

        Handles.color = handleColor;

        Handles.Label(InPos + InDir * InRadius, $"{InName}: <b>{InRadius.value}</b>", style);
        float output = Handles.ScaleSlider(
            InRadius,
            InPos + InDir * InRadius, InDir, Quaternion.identity,
            InSize, InSnap
        );

        Handles.color = handlesColor;
        return output;
    }

    private void OnSceneGUI()
    {
        WaveManager example = (WaveManager) target;
        if (!example.EchoLocationSettings)
            return;

        var settings = example.EchoLocationSettings;

        var      handlesColor = Handles.color;
        GUIStyle style        = new GUIStyle();

        float snap = 0.1f;

        Vector3 pPos = Vector3.zero;
        Vector3 fwd  = Vector3.forward;
        Vector3 rgt  = Vector3.right;
        Vector3 up   = Vector3.up;

        var player = GameObject.FindWithTag("Player");
        if (null != player)
        {
            pPos = player.transform.position;
            fwd  = player.transform.forward;
            rgt  = player.transform.right;
            up   = player.transform.up;
        }


        EditorGUI.BeginChangeCheck();

        float size = HandleUtility.GetHandleSize(pPos) * 0.5f;

        float firstRadius  = settings.FirstAreaRadius;
        float secondRadius = settings.SecondAreaRadius;
        float maxRadius    = settings.MaxRadius;

        if (settings.ShowRadiusEditorGizmos)
        {
            firstRadius = RadiusScaler(
                pPos, rgt, settings.FirstAreaRadius, "First Radius", size, snap, new Color(0, .5f,  1f),
                new Color(.5f,                                                                .75f, 1f, .8f)
            );

            secondRadius = RadiusScaler(
                pPos, rgt, settings.SecondAreaRadius, "Second Radius", size, snap, new Color(0, .5f,  1f),
                new Color(.5f,                                                                  .75f, 1f, .8f)
            );

            maxRadius = RadiusScaler(
                pPos, rgt, settings.MaxRadius, "Max Radius", size, snap, new Color(0, .5f,  1f),
                new Color(.5f,                                                        .75f, 1f, .8f)
            );
        }


        float coneInner  = settings.coneInnerRadius;
        float coneOuter  = settings.coneOuterRadius;
        float coneLength = settings.coneLength;

        if (settings.ShowConeEditorGizmos)
        {
            coneInner = RadiusScaler(
                pPos, -rgt, settings.coneInnerRadius, "Cone Inner Radius", size, snap, new Color(0, 1f, .5f),
                new Color(.5f,                                                                      1f, .75f, .8f)
            );

            coneOuter = RadiusScaler(
                pPos + fwd * settings.coneLength, -rgt,
                settings.coneOuterRadius,
                "Cone Outer Radius", size, snap, new Color(0, 1f, .5f), new Color(.5f, 1f, .75f, .8f)
            );


            Handles.color          = new Color(0,   1f, .5f);
            style.normal.textColor = new Color(.5f, 1f, .75f, .8f);
            style.richText         = true;

            var length = settings.coneLength;
            coneLength = Handles.ScaleSlider(length, pPos + fwd * length, fwd, Quaternion.identity, size, snap);
            Handles.Label(
                pPos + fwd * length,
                $"Cone Length: <b>{length.value}</b>",
                style
            );
        }


        float zoneMultiplier = settings.permanentZoneMultiplier;

        if (settings.ShowPermanentZoneEditorGizmos)
        {
            Handles.color          = new Color(0,   1f, 1f);
            style.normal.textColor = new Color(.5f, 1f, 1f, .8f);
            style.richText         = true;

            float permZoneRadius =
                settings.FirstAreaRadius * settings.permanentZoneMultiplier;

            Handles.Label(
                pPos + up * permZoneRadius,
                $"Permanent Zone Multiplier: <b>x{settings.permanentZoneMultiplier.value}</b> ({permZoneRadius})",
                style
            );

            zoneMultiplier = Handles.ScaleSlider(
                settings.permanentZoneMultiplier,
                pPos + up * permZoneRadius, up, Quaternion.identity,
                size, snap
            );
        }

        Handles.color = handlesColor;

        size = HandleUtility.GetHandleSize(example.transform.position) * 0.1f;

        var pos   = example.transform.position;
        var hsize = example.Size / 2;

        var handleX = Handles.Slider(
            pos + new Vector3(hsize.x, 0, 0), Vector3.right, size, Handles.CubeHandleCap, snap
        );

        var handleY = Handles.Slider(
            pos + new Vector3(0, 0, hsize.y), Vector3.forward, size, Handles.CubeHandleCap, snap
        );

        if (!EditorGUI.EndChangeCheck())
            return;

        Undo.RecordObject(example,  "Change Look At Target Position");
        Undo.RecordObject(settings, "Change EchoLocation Settings.");

        example.Size.x = ((handleX - pos) * 2).x;
        example.Size.y = ((handleY - pos) * 2).z;

        example.EchoLocationSettings.FirstAreaRadius.value  = firstRadius;
        example.EchoLocationSettings.SecondAreaRadius.value = secondRadius;
        example.EchoLocationSettings.MaxRadius.value        = maxRadius;

        example.EchoLocationSettings.permanentZoneMultiplier.value = zoneMultiplier;

        example.EchoLocationSettings.coneInnerRadius.value = coneInner;
        example.EchoLocationSettings.coneOuterRadius.value = coneOuter;
        example.EchoLocationSettings.coneLength.value      = coneLength;

        // example.Update();
    }
}

#endif

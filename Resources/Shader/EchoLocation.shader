Shader "Hidden/Sonar/EchoLocation"
{
    HLSLINCLUDE

    #include "Packages/com.unity.postprocessing/PostProcessing/Shaders/StdLib.hlsl"
    #include "Packages/com.unity.postprocessing/PostProcessing/Shaders/Colors.hlsl"
    #include "../../ShaderLibrary/SimplexNoise3D.hlsl"

    #define EXCLUDE_FAR_PLANE
    #pragma multi_compile __ LOOP

    // x: First, y: Second, z: Max.
    float3 _WaveRadius;
    float _WaveSpeed;
    
    float2 _OuterTransition;

    float4x4 unity_CameraInvProjection;
    float4x4 _InverseView;
    float4 unity_CameraWorldClipPlanes[6];
    
    TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);
    float4 _Maintex_TexelSize;
    
    TEXTURE2D_SAMPLER2D(_CameraDepthTexture, sampler_CameraDepthTexture);
    float4 _CameraDepthTexture_TexelSize;

    TEXTURE2D_SAMPLER2D(_PropsTexture, sampler_PropsTexture);
    float4 _PropsTexture_TexelSize;

    TEXTURE2D_SAMPLER2D(_RayMarchedDepthTexture, sampler_RayMarchedDepthTexture);
    TEXTURE2D_SAMPLER2D(_WaveUpdateTex, sampler_WaveUpdateTex);

    float3 _CharacterPos;
    float4x4 _CharacterTransform;
    
    float _PermanentZoneMultiplier;
    
    // R1, R2, L
    float3 _Cone;
    float _DirectionalWaveState;

    struct Wave
    {
        float3 position;
        float startTime;
    };
    
    int _ActiveWaves;
    
    #define MAX_WAVES 64
    float4 _Waves[MAX_WAVES]; // xyz: position, w: startTime
    
    struct Varyings
    {
        float4 position : SV_Position;
        float2 texcoord : TEXCOORD0;
        float3 ray : TEXCOORD2;
        float3 viewDir : TEXCOORD3;
    };

    // Vertex shader that procedurally outputs a full screen triangle
    Varyings Vertex(AttributesDefault v)
    {
        float3 vpos = float3(v.vertex.xy, 1);
     
        // Render settings
        float  far       = _ProjectionParams.z;
        float2 orthoSize = unity_OrthoParams.xy;
        float  isOrtho   = unity_OrthoParams.w; // 0: perspective, 1: orthographic

        // Perspective: view space vertex position of the far plane
        float3 rayPers = mul(unity_CameraInvProjection, vpos.xyzz * far).xyz;

        // Orthographic: view space vertex position
        float3 rayOrtho = float3(orthoSize * vpos.xy, 0);

        Varyings o;
        o.position = float4(vpos.x, -vpos.y, 1, 1);
        o.texcoord = (vpos.xy + 1) * 0.5;
        o.ray = lerp(rayPers, rayOrtho, isOrtho);
        o.viewDir = lerp(rayPers, rayOrtho, isOrtho);
        
        return o;
    }

    /**
     * ComputeViewSpacePosition: Returns a ViewSpace position based on a DepthMap.
     */
    float4 ComputeViewSpacePosition(Varyings input)
    {
        // Render settings
        float near    = _ProjectionParams.y;
        float far     = _ProjectionParams.z;
        float isOrtho = unity_OrthoParams.w; // 0: perspective, 1: orthographic

        // Z buffer sample
        float z = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, input.texcoord);
        
        // Far plane exclusion
#if !defined(EXCLUDE_FAR_PLANE)
        float mask = 1;
#elif defined(UNITY_REVERSED_Z)
        float mask = z > 0;
#else
        float mask = z < 1;
#endif

        // Perspective: view space position = ray * depth
        float3 vposPers = input.viewDir * Linear01Depth(z);

        // Orthographic: linear depth (with reverse-Z support)
#if defined(UNITY_REVERSED_Z)
        float depthOrtho = -lerp(far, near, z);
#else
        float depthOrtho = -lerp(near, far, z);
#endif

        // Orthographic: view space position
        float3 vposOrtho = float3(input.viewDir.xy, depthOrtho);

        // Result: view space position
        return float4(lerp(vposPers, vposOrtho, isOrtho), mask);
    }
    
    float2 ComputeDistance(Varyings input, float3 pos)
    {
        // TODO: Proper shader parameters.
        const float _Exponential = 10;
        const float _Amplitude   = 1;
        
        const float3 permRadius = _WaveRadius * _PermanentZoneMultiplier;
        
        float d = distance(_CharacterPos, pos);
        d = min(d, permRadius.z);
        
        float _LoopDuration = permRadius.z / _WaveSpeed;
    
        // Updates distance using time: wave spreading.
        float dist = d;
        
        if (d >= permRadius.z) d = 0;

        float low1  = permRadius.y;
        float high1 = permRadius.z;
        
        float low2  = 0;
        float high2 = 1;
        
//          low2 + (value - low1) * (high2 - low2) / (high1 - low1)
        dist = 1 - (low2 + (dist - low1) * (high2 - low2) / (high1 - low1));
        
        // Used to loop (manual modulo)
        // Todo: Useless if we don't loop.
        d /= permRadius.z;
        d = d - floor(d);
        
        float2 outDistance = float2(dist, d);
        for (int i = 0; i < _ActiveWaves && i < MAX_WAVES; i++)
        {
            Wave wave;
            wave.position = _Waves[i].xyz;
            wave.startTime = _Waves[i].w;
            
            // Distance from pixel wpos to center of wave.
            d = distance(wave.position, pos);
            d = min(d, _WaveRadius.z);
            
            float _LoopDuration = _WaveRadius.z / _WaveSpeed;
          
            float age = wave.startTime;  
            // Clamp: allows us to not play the effect before startTime, and won't play it after startTime + _LoopDuration.
        #if !defined(LOOP)
            age = min(age, _LoopDuration);
        #else
            age %= _LoopDuration;
        #endif
        
            // Updates distance using time: wave spreading.
            dist = d;
            
            float ageRatio = age / _LoopDuration;
            float invAgeRatio = 1 / ageRatio;
            
//            d = min(d, _WaveRadius.z * ageRatio);
            if (d >= _WaveRadius.z * ageRatio) continue;
            
            d -= age * _WaveSpeed;

            low1  = _WaveRadius.y;
            high1 = _WaveRadius.z;
            
            low2  = 0;
            high2 = 1;
            
//          low2 + (value - low1) * (high2 - low2) / (high1 - low1)
//            if (dist <= _WaveRadius.x) dist = 1.0;
//            else if (dist >= _WaveRadius.z) dist = 0.0;
//            else dist = 1 - ((dist - _WaveRadius.x) / (_WaveRadius.y - _WaveRadius.x));
            dist = 1 - (low2 + (dist - low1) * (high2 - low2) / (high1 - low1));
            
            // Used to loop (manual modulo)
            // Todo: Useless if we don't loop.
            d /= _WaveRadius.z;
            d = d - floor(d);
            
//            if (d > ageRatio)
//            {
//            continue;
//            }
            
            // SDF combine op: using max, we combine all the distance in a global distance.
            outDistance.y = max(d, outDistance.y);
            outDistance.x = (outDistance.x > 0.00001 ? max(outDistance.x, dist) : dist);
        }
        
        return outDistance;
    }

	float WaveEdge(float distance)
	{
		const float p = _WaveRadius.z * 20;
		
		if (distance < 0.00001)
		return 0;
		
		return PositivePow(1 - distance, p);
	}

	float WaveTrail(float distance)
	{
		const float p = _WaveRadius.z * 5;

		return PositivePow(distance, p);
	}

    // TODO: rename that >_>
    /**
     * GetMediumDepth: Return wether we lie on a outline or not.
     */
    bool GetDepthOutline(float2 uv)
    {
        // Depth Texture pixel's size, to ease use kernel build
        const float2 k = _CameraDepthTexture_TexelSize.xy;
        // Treshold for the outline
        const float kTreshold = 1e-4;
        
        // Outline kernel
        const float2 ks[8] = 
        {
            float2(0, k.y),
            float2(0, -k.y),
            float2(k.x, 0),
            float2(-k.x, 0),
            float2(k.x, k.y),
            float2(-k.x, k.y),
            float2(k.x, -k.y),
            float2(-k.x, -k.y)
        };
        
        // Neighbors sampling        
        float sample = 0;
        sample += SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, saturate(uv + ks[0]));
        sample += SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, saturate(uv + ks[1]));
        sample += SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, saturate(uv + ks[2]));
        sample += SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, saturate(uv + ks[3]));
        sample += SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, saturate(uv + ks[4]));
        sample += SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, saturate(uv + ks[5]));
        sample += SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, saturate(uv + ks[6]));
        sample += SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, saturate(uv + ks[7]));

        sample /= 8;

        // Current pixel
        float center = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, uv);
        
        // if the difference between current and neighbors is more than treshold, we're on a outline.
        return lerp(0, 1, step(kTreshold, center - sample));
    }
    
    float GetTextureOutline(float2 uv)
    {
        // Depth Texture pixel's size, to ease use kernel build
        const float2 k = _CameraDepthTexture_TexelSize.xy;
        // Treshold for the outline
        const float kTreshold = 1e-4;
        
        // Outline kernel
        const float2 ks[8] = 
        {
            float2(0, k.y),
            float2(0, -k.y),
            float2(k.x, 0),
            float2(-k.x, 0),
            float2(k.x, k.y),
            float2(-k.x, k.y),
            float2(k.x, -k.y),
            float2(-k.x, -k.y)
        };
        
        // Neighbors sampling        
        float sample = 0;
        sample += SAMPLE_TEXTURE2D(_PropsTexture, sampler_PropsTexture, saturate(uv + ks[0])).r;
        sample += SAMPLE_TEXTURE2D(_PropsTexture, sampler_PropsTexture, saturate(uv + ks[1])).r;
        sample += SAMPLE_TEXTURE2D(_PropsTexture, sampler_PropsTexture, saturate(uv + ks[2])).r;
        sample += SAMPLE_TEXTURE2D(_PropsTexture, sampler_PropsTexture, saturate(uv + ks[3])).r;
        sample += SAMPLE_TEXTURE2D(_PropsTexture, sampler_PropsTexture, saturate(uv + ks[4])).r;
        sample += SAMPLE_TEXTURE2D(_PropsTexture, sampler_PropsTexture, saturate(uv + ks[5])).r;
        sample += SAMPLE_TEXTURE2D(_PropsTexture, sampler_PropsTexture, saturate(uv + ks[6])).r;
        sample += SAMPLE_TEXTURE2D(_PropsTexture, sampler_PropsTexture, saturate(uv + ks[7])).r;

        sample /= 8;

        // Current pixel
        float center = SAMPLE_TEXTURE2D(_PropsTexture, sampler_PropsTexture, uv).r;
        
        // if the difference between current and neighbors is more than treshold, we're on a outline.
        return lerp(0, 1, step(kTreshold, center - sample));
    }

    float sdRoundCone( float3 p, float r1, float r2, float h )
    {
        float2 q = float2( length(p.xy), p.z );
        
        float b = (r1-r2)/h;
        float a = sqrt(1.0-b*b);
        float k = dot(q,float2(-b,a));
        
        if( k < 0.0 ) return length(q) - r1;
        if( k > a*h ) return length(q-float2(0.0,h)) - r2;
            
        return dot(q, float2(a,b) ) - r1;
    }

    ENDHLSL

    // TODO: Step by Step passes for presentation.
    SubShader
    {
        Cull Off ZWrite Off ZTest Always
        
        // Pass 0: View Space
        Pass
        {
            Name "Distance"
            
            HLSLPROGRAM

            #pragma vertex Vertex
            #pragma fragment Fragment

            float2 Fragment(Varyings input) : SV_Target
            {                
                // Get ViewSpace position from depth map.
                float4 vpos = ComputeViewSpacePosition(input);
                
                // To prevent from drawing on the far plane.
                // TODO: blending instead of branching.
                if (vpos.a < 1)
                    return 0;
                
                // Convert ViewSpace to WorldSpace.
                float3 wpos = mul(_InverseView, float4(vpos.xyz, 1)).xyz;
                
                return ComputeDistance(input, wpos);
            }

            ENDHLSL
        }
        
        // Pass 1: Depth Outline
        Pass 
        {
            Name "DepthOutline"
            
            HLSLPROGRAM
            
            #pragma vertex Vertex
            #pragma fragment Fragment
            
            float Fragment(Varyings input) : SV_Target
            {
                return GetDepthOutline(input.texcoord);
            }
               
            ENDHLSL
        }
        
        // Pass 2: Props Outline
        Pass 
        {
            Name "PropsOutline"
            
            HLSLPROGRAM
            
            #pragma vertex Vertex
            #pragma fragment Fragment
            
            float Fragment(Varyings input) : SV_Target
            {
                return GetTextureOutline(input.texcoord);
            }
               
            ENDHLSL
        }
        
        // Pass 3: Chara Outline
        Pass 
        {
            HLSLPROGRAM
            
            #pragma vertex Vertex
            #pragma fragment Fragment
            
            half4 Fragment(Varyings input) : SV_Target
            {
                return 0;
            }
               
            ENDHLSL
        }
        
        // Pass 4: Outlines Blur
        Pass 
        {
            HLSLPROGRAM
            
            #pragma vertex Vertex
            #pragma fragment Fragment
            
            TEXTURE2D_SAMPLER2D(_DepthOutlineTex, sampler_DepthOutlineTex);
            
            half4 Fragment(Varyings input) : SV_Target
            {
                return SAMPLE_TEXTURE2D(_DepthOutlineTex, sampler_DepthOutlineTex, input.texcoord);
            }
               
            ENDHLSL
        }
        
        // Pass 5: Compositing
        Pass 
        {
            Name "Compositing"
        
            HLSLPROGRAM
            
            #pragma vertex Vertex
            #pragma fragment Fragment
            
            half4 Fragment(Varyings input) : SV_Target
            {
                float4 waveUpdate   = SAMPLE_TEXTURE2D(_WaveUpdateTex, sampler_WaveUpdateTex, input.texcoord);
                float4 scene        = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.texcoord);
                
                float4 vpos = ComputeViewSpacePosition(input);
                // Convert ViewSpace to WorldSpace.
                float3 wpos = mul(_InverseView, float4(vpos.xyz, 1)).xyz;
                float4 noise = snoise_grad(wpos + float3(_SinTime.y * 10, _CosTime.y * 10, 0));
                
                float4 sceneDistort = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, input.texcoord + noise.xy / 100);
                
                float distortMask   = saturate(ceil((waveUpdate + 1) / 2));
                
                float4 levelThree = GetDepthOutline(input.texcoord + noise.xy / 10);
                float4 levelTwo = lerp(GetDepthOutline(input.texcoord), levelThree, saturate((waveUpdate.r - 2) / 2));
                float4 levelOne = lerp(scene, levelTwo, saturate((waveUpdate.r - 1) / 2));
                
                float dirWave = saturate(sdRoundCone(mul(_CharacterTransform, float4(wpos, 1)).xyz, _Cone.x, _Cone.y, _Cone.z));
                
                float4 waves = lerp(0, levelOne, saturate(waveUpdate.r));
                float permZoneSize = _WaveRadius.x * _PermanentZoneMultiplier;
                
                float4 withDirectional = lerp(scene, waves, min(dirWave, min(distance(wpos, _CharacterPos), permZoneSize) / permZoneSize));
                float4 withoutDirectional = lerp(scene, waves, min(distance(wpos, _CharacterPos), permZoneSize) / permZoneSize);
                
                return lerp(withoutDirectional, withDirectional, _DirectionalWaveState);                                
            }
               
            ENDHLSL
        }
    }
}

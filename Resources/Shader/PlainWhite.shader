﻿Shader "Unlit/PlainWhite"
{
    HLSLINCLUDE
    
    // This actually works inside HLSL shaders... figures
    #include "UnityCG.cginc"
    
    struct Attributes
    {
        float4 vertex : POSITION;
    };

    struct Varyings
    {
        float4 vertex : SV_POSITION;
    };
    
    Varyings DefaultVert(Attributes In)
    {
        Varyings Out;
        Out.vertex = UnityObjectToClipPos(In.vertex);
        
        return Out;
    }
    
    ENDHLSL
    
    SubShader
    {   
        Pass
        {
            Name "Additive"
            ZTest Always
        
            HLSLPROGRAM
            #pragma vertex DefaultVert
            #pragma fragment Fragment

            half Fragment (Varyings In) : COLOR
            {
                return 1;
            }
            ENDHLSL
        }
        
        Pass
        {
            Name "Substractive"
        
            HLSLPROGRAM
            #pragma vertex DefaultVert
            #pragma fragment Fragment

            half Fragment (Varyings In) : COLOR
            {
                return 0;
            }
            ENDHLSL
        }
    }
}

using System;
using System.Collections.Generic;

using UnityEditor;

using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[Serializable]
[UnityEngine.Rendering.PostProcessing.PostProcess(
    typeof(EchoLocationRenderer), PostProcessEvent.AfterStack, "Sonar/Echo Location"
)]
public sealed class EchoLocation : PostProcessEffectSettings
{
    [Serializable]
    public class ComputeShaderParameter : ParameterOverride<ComputeShader>
    {
        public ComputeShaderParameter()
        {
            overrideState = true;
            value         = null;
        }
    }

#if UNITY_EDITOR
    public override bool IsEnabledAndSupported(PostProcessRenderContext context) { return EditorApplication.isPlaying; }
#endif

    [Tooltip("Compute Shader utilisé pour générer les ondes.")]
    public ComputeShaderParameter computeShader = new ComputeShaderParameter();

    [Tooltip("Liste des ondes. Utiles uniquement pour debug dans l'éditeur, sinon est ré-écrit ingame.")]
    public WaveListParameter waves = new WaveListParameter {value = new List<Wave>(8)};

    [UnityEngine.Rendering.PostProcessing.Min(0),
     Tooltip("Vitesse d'une onde")]
    public FloatParameter waveSpeed = new FloatParameter {value = 5};

    [UnityEngine.Rendering.PostProcessing.MinMax(0f, 1f),
     Tooltip("Ou se trouve la transition entre le niveau 2 et 3.")]
    public Vector2Parameter OuterTransition = new Vector2Parameter {value = new Vector2(0.3f, 0.7f)};


    [Header("Zones"),
     Tooltip("Affiche ou non les gizmos pour editer les Radii.")]
    public BoolParameter ShowRadiusEditorGizmos = new BoolParameter {value = true};

    [UnityEngine.Rendering.PostProcessing.Min(0),
     Tooltip("Rayon de la Première zone, où tout est directement affiché")]
    public FloatParameter FirstAreaRadius = new FloatParameter {value = 4};

    [UnityEngine.Rendering.PostProcessing.Min(0),
     Tooltip("Rayon de la Seconde zone, où ça fade au fur et à mesure.")]
    public FloatParameter SecondAreaRadius = new FloatParameter {value = 8};

    [UnityEngine.Rendering.PostProcessing.Min(0),
     Tooltip("Distance au dela de laquelle l'onde n'est plus affichée.")]
    public FloatParameter MaxRadius = new FloatParameter {value = 16};

    [Header("Zone Permanante"),
     Tooltip("Affiche ou non les gizmos pour editer la zone permanante.")]
    public BoolParameter ShowPermanentZoneEditorGizmos = new BoolParameter {value = true};

    [UnityEngine.Rendering.PostProcessing.Min(0),
     Tooltip("Taille de la zone permanente, en multiplier de la zone de base.")]
    public FloatParameter permanentZoneMultiplier = new FloatParameter {value = .5f};

    [Header("Cone"),
     Tooltip("Affiche ou non les gizmos pour editer le cone.")]
    public BoolParameter ShowConeEditorGizmos = new BoolParameter {value = true};

    [UnityEngine.Rendering.PostProcessing.Min(0),
     Tooltip("Rayon du cone autour du perso.")]
    public FloatParameter coneInnerRadius = new FloatParameter {value = 1};

    [UnityEngine.Rendering.PostProcessing.Min(0),
     Tooltip("Rayon du cone à son extremité.")]
    public FloatParameter coneOuterRadius = new FloatParameter {value = 4};

    [UnityEngine.Rendering.PostProcessing.Min(0),
     Tooltip("Taille du cone.")]
    public FloatParameter coneLength = new FloatParameter {value = 10};

    [Tooltip("Etat du cone.")]
    public BoolParameter isConeEnabled = new BoolParameter {value = false};
}

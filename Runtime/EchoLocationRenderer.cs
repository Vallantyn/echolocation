using System.Collections.Generic;

using UnityEditor;

using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.PostProcessing;

public sealed class EchoLocationRenderer : PostProcessEffectRenderer<EchoLocation>
{
    private const int kMaxWaves = 64;

    public static int MaxWaves => kMaxWaves;

    public override DepthTextureMode GetCameraFlags() { return DepthTextureMode.Depth | DepthTextureMode.DepthNormals; }

    private Transform _PlayerXForm;
    private Transform _PlayerEyesXForm;

    public override void Init()
    {
        base.Init();

        var player     = GameObject.FindGameObjectWithTag("Player");
        var playerEyes = GameObject.FindGameObjectWithTag("Player");

        if (null != player) { _PlayerXForm = player.transform; }

        _PlayerEyesXForm = null != playerEyes ? playerEyes.transform : _PlayerXForm;
    }

    void ComputeWaveUpdate(PostProcessRenderContext InCtx, RenderTargetIdentifier InDestRT)
    {
        var RayMarcher = settings.computeShader.value;
        if (!RayMarcher)
            return;

        var cmd   = InCtx.command;
        var sheet = InCtx.propertySheets.Get(Shader.Find("Hidden/CompositeRayMarcher"));
        var waves = settings.waves.value;

//        if (waves.Count == 0)
//            return;

        var KernelIndex = RayMarcher.FindKernel("SDFMain");

        cmd.SetComputeMatrixParam(
            RayMarcher, "_CameraToWorld", InCtx.camera.cameraToWorldMatrix
        );

        cmd.SetComputeMatrixParam(
            RayMarcher, "_CameraInverseProjection", InCtx.camera.projectionMatrix.inverse
        );

        var player = GameObject.FindGameObjectWithTag("Player");
        if (null != player)
            cmd.SetComputeVectorParam(
                RayMarcher, "_CharacterPosition", player.transform.position
            );

        cmd.SetComputeVectorParam(
            RayMarcher, "_CameraWorldPosition", InCtx.camera.transform.position
        );

        cmd.SetComputeVectorParam(
            RayMarcher, "_AreaRect", new Vector4
            {
                x = WaveManager.Instance.transform.position.x,
                y = WaveManager.Instance.transform.position.z,
                z = WaveManager.Instance.Size.x,
                w = WaveManager.Instance.Size.y
            }
        );

        RayMarcher.SetTextureFromGlobal(KernelIndex, "_Depth",   "_CameraDepthTexture");
        RayMarcher.SetTextureFromGlobal(KernelIndex, "_WaveMap", "_WaveMap");

        var resId   = Shader.PropertyToID("WaveUpdateResult");
        var resRTid = new RenderTargetIdentifier(resId);

        cmd.GetTemporaryRT(
            resId, Screen.width, Screen.height, 0, FilterMode.Point, RenderTextureFormat.DefaultHDR,
            RenderTextureReadWrite.Linear, 1, true
        );

        cmd.SetComputeTextureParam(RayMarcher, KernelIndex, resId, resRTid);

        // Set the target and dispatch the compute shader
//        RayTracingShader.SetTexture(0, "Result", _Target);
        var threadGroupsX = Mathf.CeilToInt(Screen.width  / 8.0f);
        var threadGroupsY = Mathf.CeilToInt(Screen.height / 8.0f);

//        var buffer = new ComputeBuffer(waves.Count, 16);
//        buffer.SetData(waves);

//        RayMarcher.SetBuffer(KernelIndex, "Waves", buffer);
//        var threadGroupsZ = Mathf.CeilToInt(waves.Count / 8.0f);

        RayMarcher.Dispatch(KernelIndex, threadGroupsX, threadGroupsY, 1);

        cmd.BlitFullscreenTriangle(resRTid, InDestRT, true);

        cmd.ReleaseTemporaryRT(resId);

//        buffer.Release();
    }

    void SetupShaderProperties(MaterialPropertyBlock InProperties, PostProcessRenderContext InContext)
    {
        Shader.SetGlobalVector(
            ShaderIDs.Radius, new Vector4(settings.FirstAreaRadius, settings.SecondAreaRadius, settings.MaxRadius)
        );

        InProperties.SetFloat(ShaderIDs.PermanentZoneMultiplier, settings.permanentZoneMultiplier);
        InProperties.SetVector(
            "_Cone", new Vector3(settings.coneInnerRadius, settings.coneOuterRadius, settings.coneLength)
        );

        InProperties.SetMatrix("_InverseView", InContext.camera.cameraToWorldMatrix);

        InProperties.SetVector("_CharacterPos", _PlayerXForm.position);
        InProperties.SetMatrix("_CharacterTransform", _PlayerEyesXForm.worldToLocalMatrix);
        InProperties.SetFloat("_DirectionalWaveState", settings.isConeEnabled ? 1 : 0);
    }

    public override void Render(PostProcessRenderContext InContext)
    {
        if (!settings)
            return;

        var cmd = InContext.command;

        // Main FX
        cmd.BeginSample("Inverse Projection");

        var sheet = InContext.propertySheets.Get(Shader.Find("Hidden/Sonar/EchoLocation"));
        SetupShaderProperties(sheet.properties, InContext);

        // Compute Weevy
        {
            var id   = Shader.PropertyToID("_WaveUpdateTex");
            var rtId = new RenderTargetIdentifier(id);
            cmd.GetTemporaryRT(id, Screen.width, Screen.height, 0, FilterMode.Point, RenderTextureFormat.DefaultHDR);

            ComputeWaveUpdate(InContext, rtId);
        }

        // Final Blit
        cmd.BlitFullscreenTriangle(InContext.source, InContext.destination, sheet, 5);

        cmd.EndSample("Inverse Projection");
    }

    public override void Release() { base.Release(); }

    private static class ShaderIDs
    {
        internal static readonly int InverseView = Shader.PropertyToID("_InverseView");
        internal static readonly int ActiveWaves = Shader.PropertyToID("_ActiveWaves");

        internal static readonly int Radius = Shader.PropertyToID("_WaveRadius"); // x: First, y: Second, z: Max.

        internal static readonly int
            PermanentZoneMultiplier = Shader.PropertyToID("_PermanentZoneMultiplier"); // x: First, y: Second, z: Max.

        internal static readonly int Speed = Shader.PropertyToID("_WaveSpeed");

        internal static readonly int OuterTransition = Shader.PropertyToID("_OuterTransition");
    }

    private static class RTFormat
    {
        public static readonly RenderTextureDescriptor FullRIntD0 = new RenderTextureDescriptor(
            Screen.width, Screen.height, RenderTextureFormat.RInt, 0
        );

        public static readonly RenderTextureDescriptor FullRGFloatD0 = new RenderTextureDescriptor(
            Screen.width, Screen.height, RenderTextureFormat.RGFloat, 0
        );
    }
}

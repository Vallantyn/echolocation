using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[Serializable]
public struct Wave
{
    public Vector3 position;
    public float   startTime;
}

[Serializable]
public sealed class WaveListParameter : ParameterOverride<List<Wave>>
{
    public WaveListParameter()
    {
        value         = new List<Wave>();
        overrideState = false;
    }
}

[Serializable]
public sealed class LayerParameter : ParameterOverride<int>
{
    public LayerParameter()
    {
        value = 0;
        overrideState = false;
    }
}

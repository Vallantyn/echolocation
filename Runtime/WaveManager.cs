using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

using Content.Scenes.ComputeSandbox.Runtime;

using UnityEditor;

using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[RequireComponent(typeof(WaveMap))]
public class WaveManager : MonoBehaviour
{
    private static WaveManager sInstance;

    private EchoLocation _EchoLocationSettings;
    public  EchoLocation EchoLocationSettings => _EchoLocationSettings;

    [SerializeField]
    private PostProcessVolume _PostProcessVolume;

    public Vector2 Size = new Vector2(100, 100);

    public static WaveManager Instance
    {
        get
        {
            if (null == sInstance)
                sInstance = FindObjectOfType<WaveManager>();

            if (null == sInstance)
                sInstance = new GameObject("[Wave Manager]").AddComponent<WaveManager>();

            return sInstance;
        }
    }

    public void Init() { }

    private void OnDrawGizmosSelected()
    {
        var pos = transform.position;

        var gcolor = Gizmos.color;
        Gizmos.color = Color.red;

        var hsize = Size / 2;

        Gizmos.DrawLine(pos + new Vector3(-hsize.x, 0, hsize.y),  pos + new Vector3(hsize.x,  0, hsize.y));
        Gizmos.DrawLine(pos + new Vector3(-hsize.x, 0, -hsize.y), pos + new Vector3(hsize.x,  0, -hsize.y));
        Gizmos.DrawLine(pos + new Vector3(hsize.x,  0, -hsize.y), pos + new Vector3(hsize.x,  0, hsize.y));
        Gizmos.DrawLine(pos + new Vector3(-hsize.x, 0, -hsize.y), pos + new Vector3(-hsize.x, 0, hsize.y));

        Vector3 pPos = Vector3.zero;
        Vector3 fwd  = Vector3.forward;
        Vector3 rgt  = Vector3.right;

        var player = GameObject.FindWithTag("Player");
        if (null != player)
        {
            pPos = player.transform.position;
            fwd  = player.transform.forward;
            rgt  = player.transform.right;
        }

        if (null == _PostProcessVolume)
            _PostProcessVolume = FindObjectOfType<PostProcessVolume>();

        if (!_EchoLocationSettings)
            _PostProcessVolume.profile.TryGetSettings(out _EchoLocationSettings);

        if (_EchoLocationSettings.ShowRadiusEditorGizmos)
        {
            Gizmos.color = new Color(0, .5f, 1, .4f);
            Gizmos.DrawWireSphere(pPos, _EchoLocationSettings.FirstAreaRadius);
            Gizmos.DrawWireSphere(pPos, _EchoLocationSettings.SecondAreaRadius);
            Gizmos.DrawWireSphere(pPos, _EchoLocationSettings.MaxRadius);
        }

        if (_EchoLocationSettings.ShowPermanentZoneEditorGizmos)
        {
            Gizmos.color = new Color(0, 1f, 1f, .4f);
            Gizmos.DrawWireSphere(
                pPos, _EchoLocationSettings.FirstAreaRadius * _EchoLocationSettings.permanentZoneMultiplier
            );
        }

        if (_EchoLocationSettings.ShowConeEditorGizmos)
        {
            Gizmos.color = new Color(0, 1f, .5f, .4f);
            Gizmos.DrawWireSphere(pPos,                                          _EchoLocationSettings.coneInnerRadius);
            Gizmos.DrawWireSphere(pPos + fwd * _EchoLocationSettings.coneLength, _EchoLocationSettings.coneOuterRadius);

            Gizmos.color = new Color(0, 1f, .5f, .4f);
            Gizmos.DrawLine(
                pPos                                          + rgt * _EchoLocationSettings.coneInnerRadius,
                pPos + fwd * _EchoLocationSettings.coneLength + rgt * _EchoLocationSettings.coneOuterRadius
            );

            Gizmos.DrawLine(
                pPos                                          - rgt * _EchoLocationSettings.coneInnerRadius,
                pPos + fwd * _EchoLocationSettings.coneLength - rgt * _EchoLocationSettings.coneOuterRadius
            );

            Gizmos.DrawLine(pPos, pPos + fwd * _EchoLocationSettings.coneLength);
        }

        Gizmos.color = gcolor;
    }

    private void OnDrawGizmos()
    {
        var pos = transform.position;

        var gcolor = Gizmos.color;
        Gizmos.color = new Color(1, 0, 0, .2f);

        var hsize = Size / 2;

        Gizmos.DrawLine(pos + new Vector3(-hsize.x, 0, hsize.y),  pos + new Vector3(hsize.x,  0, hsize.y));
        Gizmos.DrawLine(pos + new Vector3(-hsize.x, 0, -hsize.y), pos + new Vector3(hsize.x,  0, -hsize.y));
        Gizmos.DrawLine(pos + new Vector3(hsize.x,  0, -hsize.y), pos + new Vector3(hsize.x,  0, hsize.y));
        Gizmos.DrawLine(pos + new Vector3(-hsize.x, 0, -hsize.y), pos + new Vector3(-hsize.x, 0, hsize.y));

        Vector3 pPos = Vector3.zero;
        Vector3 fwd  = Vector3.forward;
        Vector3 rgt  = Vector3.right;

        var player = GameObject.FindWithTag("Player");
        if (null != player)
        {
            pPos = player.transform.position;
            fwd  = player.transform.forward;
            rgt  = player.transform.right;
        }

        if (null == _PostProcessVolume)
            _PostProcessVolume = FindObjectOfType<PostProcessVolume>();

        if (!_EchoLocationSettings)
            _PostProcessVolume.profile.TryGetSettings(out _EchoLocationSettings);

        if (_EchoLocationSettings.ShowRadiusEditorGizmos)
        {
            Gizmos.color = new Color(0, .5f, 1f, .2f);
            Gizmos.DrawSphere(pPos, _EchoLocationSettings.FirstAreaRadius);
            Gizmos.DrawSphere(pPos, _EchoLocationSettings.SecondAreaRadius);
            Gizmos.DrawSphere(pPos, _EchoLocationSettings.MaxRadius);
        }

        if (_EchoLocationSettings.ShowPermanentZoneEditorGizmos)
        {
            Gizmos.color = new Color(0, 1f, 1f, .2f);
            Gizmos.DrawSphere(
                pPos, _EchoLocationSettings.FirstAreaRadius * _EchoLocationSettings.permanentZoneMultiplier
            );
        }

        if (_EchoLocationSettings.ShowConeEditorGizmos)
        {
            Gizmos.color = new Color(0, 1f, .5f, .2f);
            Gizmos.DrawSphere(pPos,                                          _EchoLocationSettings.coneInnerRadius);
            Gizmos.DrawSphere(pPos + fwd * _EchoLocationSettings.coneLength, _EchoLocationSettings.coneOuterRadius);

            Gizmos.color = new Color(0, 1f, .5f, .2f);
            Gizmos.DrawLine(
                pPos                                          + rgt * _EchoLocationSettings.coneInnerRadius,
                pPos + fwd * _EchoLocationSettings.coneLength + rgt * _EchoLocationSettings.coneOuterRadius
            );

            Gizmos.DrawLine(
                pPos                                          - rgt * _EchoLocationSettings.coneInnerRadius,
                pPos + fwd * _EchoLocationSettings.coneLength - rgt * _EchoLocationSettings.coneOuterRadius
            );
        }

        Gizmos.color = gcolor;
    }

    public Wave[] GetWaves() { return _EchoLocationSettings.waves.value.ToArray(); }

    /// <summary>
    /// Emits a wave at position, with optional delay.
    /// </summary>
    /// <param name="position">Position to emit the wave from.</param>
    /// <param name="delay">Delay after which the wave will be emitted.</param>
    public void EmitWave(Vector3 position, float delay = 0f)
    {
        var ShaderTime = Shader.GetGlobalVector("_Time");

        _EchoLocationSettings.waves.value.Add(
            new Wave
            {
                position  = position,
                startTime = 0
            }
        );

        while (_EchoLocationSettings.waves.value.Count > EchoLocationRenderer.MaxWaves)
            _EchoLocationSettings.waves.value.Remove(
                _EchoLocationSettings.waves.value
                                     .OrderBy(
                                          wave => wave.startTime +
                                                  (_EchoLocationSettings.MaxRadius /
                                                   _EchoLocationSettings.waveSpeed)
                                      )
                                     .First()
            );
    }

    private bool _DirectionalWaveState
    {
        set => _EchoLocationSettings.isConeEnabled.value = value;
        get => _EchoLocationSettings.isConeEnabled.value;
    }

    public bool DirectionalWaveState => _DirectionalWaveState;

    /// <summary>
    /// Set the Directional wave state.
    /// </summary>
    /// <param name="InValue">New value for directional.</param>
    public void SetDirectional(bool InValue) => _DirectionalWaveState = InValue;

    /// <summary>
    /// Show the Directional wave.
    /// </summary>
    public void ShowDirectional() => _DirectionalWaveState = true;

    /// <summary>
    /// Hides the Directional wave.
    /// </summary>
    public void HideDirectional() => _DirectionalWaveState = false;

    /// <summary>
    /// Toggle the Directional Wave state.
    /// </summary>
    public void ToggleDirectional() => _DirectionalWaveState = !_DirectionalWaveState;

    // Start is called before the first frame update
    private void OnEnable()
    {
        if (null == _PostProcessVolume)
            _PostProcessVolume = FindObjectOfType<PostProcessVolume>();

        if (!_PostProcessVolume.profile.TryGetSettings(out _EchoLocationSettings))
        {
            _EchoLocationSettings = _PostProcessVolume.profile.AddSettings<EchoLocation>();
        }

        _EchoLocationSettings.waves.Override(new List<Wave>());
    }

    // Update is called once per frame
    private void Update()
    {
        var waves       = _EchoLocationSettings.waves.value.ToArray();
        var currentTime = Shader.GetGlobalVector("_Time").y;

        foreach (var wave in waves)
        {
            if (wave.startTime >= (_EchoLocationSettings.MaxRadius / _EchoLocationSettings.waveSpeed))
            {
                _EchoLocationSettings.waves.value.Remove(wave);
            }
        }

        for (var Index = 0; Index < _EchoLocationSettings.waves.value.Count; Index++)
        {
            var wave = _EchoLocationSettings.waves.value[Index];
            wave.startTime += Time.deltaTime; // * _EchoLocationSettings.waveSpeed.value;

            _EchoLocationSettings.waves.value[Index] = wave;
        }
    }
}

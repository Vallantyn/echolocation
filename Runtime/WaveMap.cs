using UnityEngine;
using UnityEngine.UI;

namespace Content.Scenes.ComputeSandbox.Runtime
{
    public class WaveMap : MonoBehaviour
    {
        [SerializeField,
         Tooltip("Utilisé pour debugger la map générée.")]
        private UnityEngine.UI.RawImage _WaveMapImage;

        [SerializeField,
         Tooltip("Compute Shader utilisé pour générer les ondes.")]
        private ComputeShader _ComputeShader;

        private RenderTexture _WaveMapRT;
        private GameObject    _Player;

        private const int TEX_SIZE = 2048;

        private void Start()
        {
            if (!_WaveMapImage)
                _WaveMapImage = GetComponent<RawImage>();
            
            _WaveMapRT = new RenderTexture(
                TEX_SIZE, TEX_SIZE, 0, RenderTextureFormat.DefaultHDR, RenderTextureReadWrite.Linear
            )
            {
                filterMode        = FilterMode.Trilinear,
                antiAliasing      = 8,
                anisoLevel        = 3,
                enableRandomWrite = true
            };

            _WaveMapRT.Create();

            if (!_ComputeShader)
                return;

            var KernelIndex = _ComputeShader.FindKernel("WaveMap_Default");

            _Player = GameObject.FindGameObjectWithTag("Player");

            var waveMapId = Shader.PropertyToID("_WaveMap");
            _ComputeShader.SetTexture(KernelIndex, waveMapId, _WaveMapRT);

            // Set the target and dispatch the compute shader
//        RayTracingShader.SetTexture(0, "Result", _Target);
            var threadGroupsX = Mathf.CeilToInt(TEX_SIZE / 8.0f);
            var threadGroupsY = Mathf.CeilToInt(TEX_SIZE / 8.0f);

            _ComputeShader.Dispatch(KernelIndex, threadGroupsX, threadGroupsY, 1);
            Shader.SetGlobalTexture("_WaveMap", _WaveMapRT);

            if (_WaveMapImage)
                _WaveMapImage.texture = _WaveMapRT;
        }

        public void UpdateTexture(RenderTexture InWaveMap)
        {
            if (_WaveMapImage)
                _WaveMapImage.texture = InWaveMap;
        }

        private void Update()
        {
            if (!_ComputeShader)
                return;

            var waves = WaveManager.Instance.GetWaves();

            if (waves.Length == 0)
                return;

            var KernelIndex = _ComputeShader.FindKernel("WaveMap");

            var waveMapId = Shader.PropertyToID("_WaveMap");
            _ComputeShader.SetTexture(KernelIndex, waveMapId, _WaveMapRT);

            var threadGroupsX = Mathf.CeilToInt(TEX_SIZE / 8.0f);
            var threadGroupsY = Mathf.CeilToInt(TEX_SIZE / 8.0f);

            var buffer = new ComputeBuffer(waves.Length, 16);
            buffer.SetData(waves);

            _ComputeShader.SetBuffer(KernelIndex, "Waves", buffer);

            _ComputeShader.Dispatch(KernelIndex, threadGroupsX, threadGroupsY, 1);
            Shader.SetGlobalTexture("_WaveMap", _WaveMapRT);

            UpdateTexture(_WaveMapRT);

            buffer.Release();
        }

        private void OnApplicationQuit() { _WaveMapRT.Release(); }
    }
}
